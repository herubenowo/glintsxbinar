const { Pemasok } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator')

class PemasokController {

    async getAll(req, res) {
        Pemasok.findAll({
            attributes: ['id', 'nama', ['createdAt', 'waktu']]
        }).then(pemasok => {
            res.json(pemasok)
        })
    }

    async getOne(req, res) {
        Pemasok.findOne({
            where: {
                id: req.params.id
            },
            attributes: ['id', 'nama', ['createdAt', 'waktu']]
        }).then(pemasok => {
            res.json(pemasok)
        })
    }

    async create(req, res) {
        Pemasok.create({
            nama: req.body.nama
        }).then(newPemasok => {
            res.json({
                "status": "success!",
                "message": "new pemasok added to the database",
                "data": newPemasok
            })
        })
    }

    async update(req, res) {
        var update = {
            nama: req.body.nama
        }

        Pemasok.update(update, {
            where: {
                id: req.params.id
            }
        }).then(affectedRow => {
            return Pemasok.findOne({
                where: {
                    id: req.params.id
                }
            })
        }).then(p => {
            res.json({
                "status": "success!",
                "message": "pemasok updated in the database",
                "data": p
            })
        })
    }

    async delete(req, res) {
        Pemasok.destroy({
            where: {
                id: req.params.id
            }
        }).then(affectedRow => {
            if(affectedRow) {
                return {
                    "status": "success!",
                    "message": "pemasok deleted in the database",
                    "data": null
                }
            }

            return {
                "status": "error",
                "message": "failed to delete pemasok",
                "data": null
            }
        }).then(r => {
            res.json(r)
        })
    }
}

module.exports = new PemasokController