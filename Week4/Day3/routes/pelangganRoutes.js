const express = require('express')
const router = express.Router()
const PelangganController = require('../controllers/pelangganController.js')

router.get('/', PelangganController.getAll)
router.get('/:id', PelangganController.getOne)
router.post('/create', PelangganController.create) 
router.put('/update/:id', PelangganController.update)
router.delete('/delete/:id', PelangganController.delete)

module.exports = router