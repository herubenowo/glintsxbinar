const connection = require('../models/connection.js') // import connection

class TransaksiController {

    async getAll(req, res) {
        try {
            var sql = "select t.id, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total from transaksi t join barang b on t.id_barang = b.id join pelanggan p on t.id_pelanggan = p.id order by t.id"

            connection.query(sql, function(err, result) {
                if (err) throw err

                res.json({
                    status: "success",
                    data: result
                })
            })
        } catch (e) {
            res.json({
                status: "error"
            })
        }
    }

    async getOne(req, res) {
        try {
            var sql = "select t.id, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total from transaksi t join barang b on t.id_barang = b.id join pelanggan p on t.id_pelanggan = p.id where t.id = ?"

            connection.query(sql, [req.params.id], function(err, result) {
                if (err) throw err

                res.json({
                    status: "success",
                    data: result[0]
                })
            })
        } catch (e) {
            res.json({
                status: "error"
            })
        }
    }

    async create(req, res) {
        try {
            var sql = "SELECT harga FROM barang where id = ?"

            connection.query(sql, [req.body.id_barang], function(err, result) {
              if (err) {
                res.json({
                  status: "Error",
                  error: err
                });
              }
      
              var total = result[0].harga * req.body.jumlah
      
              var sqlInsert = 'INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)'
      
              connection.query(
                sqlInsert,
                [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total],
                (err, result) => {
                  if (err) {
                    res.json({
                      status: "Error",
                      error: err
                    });
                  }
                 
                  var sqlSelect = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" 

                  connection.query(sqlSelect, [result.insertId], function(err, result) {
                    if (err) {
                      res.json({
                        status: "Error",
                        error: err
                      });
                    } 

                    res.json({
                      status: "success",
                      data: result[0]
                    })
                  });
                }
              )
            });
          } catch (err) {
            
            res.json({
              status: "Error",
              error: err
            })
          }
        }

    async update(req, res) {
        try {
            var sql = "SELECT harga FROM barang where id = ?"

            connection.query(sql, [req.body.id_barang], function(err, result) {
              if (err) {
                res.json({
                  status: "Error",
                  error: err
                });
              }
      
              var total = result[0].harga * req.body.jumlah
      
              var sqlUpdate = 'UPDATE transaksi t SET id_barang = ?, id_pelanggan = ?, jumlah = ?, total = ? WHERE id = ?'
      
              connection.query(
                sqlUpdate,
                [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total, req.params.id],
                (err, result) => {
                  if (err) {
                    res.json({
                      status: "Error",
                      error: err
                    });
                  }
                        
                  var sqlSelect = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" // make an query varible

                  connection.query(sqlSelect, [req.params.id], function(err, result) {
                    if (err) {
                      res.json({
                        status: "Error",
                        error: err
                      });
                    } 

                    res.json({
                      status: "success",
                      data: result[0]
                    })
                  });
                }
              )
            });
          } catch (err) {
            
            res.json({
              status: "Error",
              error: err
            })
          }
        }
    

    async delete(req, res) {
        try {

            var sql = 'DELETE FROM transaksi t WHERE id = ?'

            connection.query(sql, [req.params.id], (err, result) => {
                if (err) {
                    res.json({
                    status: "Error",
                    error: err
                    });
                }

                res.json({
                status: 'Success',
                data: result
                })
            })
        } catch (err) {

                res.json({
                status: "Error",
                error: err
                })
        }
    }
}

module.exports = new TransaksiController;