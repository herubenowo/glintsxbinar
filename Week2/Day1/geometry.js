const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

function calculateCubeVolume(CubeLength) {
    var CubeSquareArea = CubeLength * CubeLength;
    return CubeSquareArea * CubeLength;
}

function calculateBeamVolume(BeamLength, BeamWidth, BeamHeight) {
    var BeamSquareArea = BeamLength * BeamWidth;
    return BeamSquareArea * BeamHeight;
}
function calculateTriangularPrismVolume(TriangleBase, TriangleHeight, PrismHeight) {
    var PrismTriangleArea = TriangleBase * TriangleHeight / 2;
    return PrismTriangleArea * PrismHeight;
}



function Menu() {
    rl.question(`Your answer: `, YourAnswer => {
        if (YourAnswer == 1) {
            inputCubeLength()
        } else if (YourAnswer == 2) {
            inputBeamLength()
        } else if(YourAnswer == 3) {
            inputTriangleBase()
        } else {
            console.log("Your answer must be 1, 2, or 3");
            Menu();
        }

    })
    
}

function inputCubeLength(){
    rl.question(`Cube Length: `, CubeLength => {
        if (!isNaN(CubeLength)) {
            console.log(`Cube Volume: ${calculateCubeVolume(CubeLength)}\n`);
            rl.close();
        } else {
            console.log(`Cube Length must be a number\n`)
            inputCubeLength();
        }
    })
}

function inputBeamLength(){
    rl.question(`Beam Length: `, BeamLength => {
        if(!isNaN(BeamLength)) {
            inputBeamWidth(BeamLength);
        } else {
            console.log(`Beam Length must be a number\n`);
            inputBeamLength();
        }
    })
}

function inputBeamWidth(BeamLength){
    rl.question(`Beam Width: `, BeamWidth => {
        if(!isNaN(BeamWidth)) {
            inputBeamHeight(BeamLength, BeamWidth)
        } else {
            console.log(`Beam Length must be a number\n`);
            inputBeamWidth(BeamLength);
        }
    })
}

function inputBeamHeight(BeamLength, BeamWidth){
    rl.question(`Beam Height: `, BeamHeight =>{
        if(!isNaN(BeamHeight)) {
            console.log(`Beam Volume: ${calculateBeamVolume(BeamLength, BeamWidth, BeamHeight)}\n`);
            rl.close();
        } else {
            console.log(`Beam Height must be a number\n`);
            inputBeamHeight(BeamLength, BeamWidth);
        }
    })
}

function inputTriangleBase(){
    rl.question(`Triangle Base: `, TriangleBase => {
        if(!isNaN(TriangleBase)) {
            inputTriangleHeight(TriangleBase);
        } else {
            console.log(`Triangle Base must be a number\n`);
            inputTriangleBase();
        }
    })
}

function inputTriangleHeight(TriangleBase){
    rl.question(`Triangle Height: `, TriangleHeight => {
        if(!isNaN(TriangleHeight)) {
            inputPrismHeight(TriangleBase, TriangleHeight)
        } else {
            console.log(`Triangle Height must be a number\n`);
            inputTriangleHeight(TriangleBase);
        }
    })
}

function inputPrismHeight(TriangleBase, TriangleHeight){
    rl.question(`Prism Height: `, PrismHeight =>{
        if(!isNaN(PrismHeight)) {
            console.log(`Triangular Prism Volume: ${calculateTriangularPrismVolume(TriangleBase, TriangleHeight, PrismHeight)}\n`);
            rl.close();
        } else {
            console.log(`Prism Height must be a number\n`);
            inputPrismHeight(TriangleBase, TriangleHeight);
        }
    })
}

console.log("by : Heru Benowo (0092020011)");
console.log("---------------------------");
console.log("| What do you want to do? |");
console.log("---------------------------");
console.log("1. Calculate Cube Volume");
console.log("2. Calculate Beam Volume");
console.log("3. Calculate Triangular Prism Volume");
Menu();
