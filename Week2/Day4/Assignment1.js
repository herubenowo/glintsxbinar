let fridge = ["tomato", "broccoli", "kale", "cabbage", "apple"]

for (let i = 0; i < fridge.length; i++) {
    if (!fridge[i].includes(`apple`)) {
        console.log(`${fridge[i]} is a healthy food, it's definitely worth to eat`);
    } 
}