const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let corona = [
    {name: `Sahlan`, status: `Positive`}, 
    {name: `Fahmi`, status: `Positive`}, 
    {name: `Pudji`, status: `Suspect`},
    {name: `Sherary`, status: `Suspect`},
    {name: `Kevin`, status: `Negative`},
    {name:`Heru`, status: `Negative`}];

function menu() {
    console.log(`Menu`);
    console.log(`====`);
    console.log(`1. Positive's Patient`);
    console.log(`2. Suspect's Patient`);
    console.log(`3. Negative's Patient`);
    console.log(`4. Exit`);
    rl.question(`Your input: `, input => {
        switch (Number(input)) {
            case 1:
                inputPositive();
                console.log();
                menu();
                break;
            case 2:
                inputSuspect();
                console.log();
                menu();
                break;
            case 3:
                inputNegative();
                console.log();
                menu();
                break;
            case 4:
                process.exit();
            default:
                console.log(`Your input must be 1 - 4 !\n`);
                menu();
        }
    })
}

function inputPositive() {
    var a = 0
    console.log(`Positive's Patients List`);
    console.log(`========================`);
    for (let i = 0; i < corona.length; i++) {
        if (corona[i].status.includes(`Positive`)) {
            a++
            console.log(`${a}. ${corona[i].name} is Positive`);
        }
    }
}

function inputSuspect() {
    var a = 0
    console.log(`Suspect's Patients List`);
    console.log(`=======================`);
    for (let i = 0; i < corona.length; i++) {
        if (corona[i].status.includes(`Suspect`)) {
            a++
            console.log(`${a}. ${corona[i].name} is a Suspect`);
        }
    }
}

function inputNegative() {
    var a = 0
    console.log(`Negative's Patients List`);
    console.log(`========================`);
    for (let i = 0; i < corona.length; i++) {
        if (corona[i].status.includes(`Negative`)) {
            a++
            console.log(`${a}. ${corona[i].name} is Negative, Alhamdulillah Ya Allah!!!`);
        }
    }
}

menu();