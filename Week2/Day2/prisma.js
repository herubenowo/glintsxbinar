const index = require('./index.js');


function prismVolume (length, width, height) {
    const baseArea = 0.5 * length * width;
    return baseArea * height;
}


function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null
};



function input(){
    index.rl.question('Length: ', length => {
        index.rl.question('Width: ', width => {
            index.rl.question('Height: ', height =>{
                if (!isNaN(length) && !isEmptyOrSpaces(length) &&  !isNaN(width) && !isEmptyOrSpaces(width) &&  !isNaN(height) && !isEmptyOrSpaces(height) ){
                    console.log(`\nPrismVolume: ${prismVolume(length,width,height)}`);
                    index.rl.close()
                } else {
                    console.log(`length, width, height must be a number\n`);
                    input()
                }

            })  

            })

        })
    }

module.exports.input = input
