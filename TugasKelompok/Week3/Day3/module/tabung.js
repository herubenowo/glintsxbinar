//assignment day 13 (week3) (amanda)

const bangunruang = require ('./bangunruang.js');


class tabung extends bangunruang {
    constructor(radius, height) {
        super('Tabung')

        this.radius = radius
        this.height = height
    }

    menghitungluaspermukaan () {
        return 2 * Math.PI * this.radius * (this.radius + this.height)
    }

    menghitungvolume () {
        return (Math.PI * Math.pow(this.radius, 2) * this.height)
    }

    
}
module.exports = tabung;

