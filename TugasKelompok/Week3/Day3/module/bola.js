const bangungruang =require ('./bangunruang.js')
class bola extends bangungruang {
    constructor(radius){
        super('bola')
        this.radius = radius
    }

    menghitungvolume() {
        return (4/3)*Math.PI* this.radius**3
    }

    menghitungluaspermukaan() {
        return 4*Math.PI* this.radius**2
    }
}

module.exports = bola 