// import modul
const modul = require('./module/module.js');

// instansiasi modul
const hitung = new modul()

/* Use hitung */

// Menghitung Luas Bangun Datar
console.log(`Menghitung Luas Bangun Datar`);
console.log(hitung.menghitungLuasPersegi(11))
console.log(hitung.menghitungLuasPersegiPanjang(20, 20));
console.log(hitung.menghitungLuasLingkaran(14));
console.log();
// Menghitung Keliling Bangun Datar
console.log(`Menghitung Keliling Bangun Datar`);
console.log(hitung.menghitungKelilingPersegi(40));
console.log(hitung.menghitungKelilingPersegiPanjang(100, 50));
console.log(hitung.menghitungKelilingLingkaran(21));
console.log();
//Menghitung Luas Permukaan Bangun Ruang
console.log(`Menghitung Luas Permukaan Bangun Ruang`);
console.log(hitung.menghitungLuasBalok(20,30,40));
console.log(hitung.menghitungLuasBola(20));
console.log(hitung.menghitungLuasTabung(12,15));
console.log();
// Menghitung Volume Bangun Ruang
console.log(`Menghitung Volume Bangun Ruang`);
console.log(hitung.menghitungVolumeBalok(20,30,40));
console.log(hitung.menghitungVolumeBola(20));
console.log(hitung.menghitungVolumeTabung(12,15));

/* End use hitung */