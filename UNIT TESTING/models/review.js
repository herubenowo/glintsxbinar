const mongoose = require('mongoose');
// const mongoosePaginate = require('mongoose-paginate-v2');
// const reviewModel = mongoose.model('ReviewModel', reviewSchema);
const mongoose_delete = require('mongoose-delete');
const ReviewSchema = new mongoose.Schema({
  /* your schema definition */
  comment: {
    type: String,
    required: true
  },
  rating: {
    type: Number,
    required: true,
    default: null,
    min: 0,
    max: 10
  },
  movie: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  }, 
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false,
  toJSON: { getters: true }
});

ReviewSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});
module.exports = review = mongoose.model('review', ReviewSchema, 'review');
