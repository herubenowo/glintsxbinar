let mongoose = require('mongoose')
let { user, movie, review } = require('../models')
let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../index.js')
let should = chai.should()
let fs = require('fs')
let authentication_key
let movieTitle
let userId

chai.use(chaiHttp)

describe('USER API', () => {

    describe('/POST signup', () => {
        it('It should create a user and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/signup')
                .send({
                    fullName: "Testing",
                    email: "testing@test.com",
                    password: "123456789",
                    passwordConfirmation: "123456789"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Signup success!')
                    res.body.should.have.property('token')
                    done()
                })
        })
    })

    describe('/POST signup admin', () => {
        it('It should create a user as an admin and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/admin/signup')
                .send({
                    fullName: "Admin Testing",
                    email: "admin@test.com",
                    password: "123456789",
                    passwordConfirmation: "123456789"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Signup success!')
                    res.body.should.have.property('token')
                    done()
                })
        })
    })

    describe('/POST login', () => {
        it('It should make user login and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/login')
                .send({
                    email: "testing@test.com",
                    password: "123456789"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Login success!')
                    res.body.should.have.property('token')
                    authentication_key = res.body.token
                    done()
                })
        })
    })

    describe('/GET own profile', () => {
        it('It should get your own profile', (done) => {
            chai.request(server)
                .get('/user/profile')
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('success')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('userId')
                    res.body.data.should.have.property('fullName')
                    res.body.data.should.have.property('email')
                    res.body.data.should.have.property('role')
                    res.body.data.should.have.property('image')
                    userId = res.body.data.userId
                    done()
                })
        })
    })

    describe('/GET one profile', () => {
        it('It should get one user profile', (done) => {
            chai.request(server)
                .get('/user/profile/get')
                .query({
                    userId: userId
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Get one user profile success')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('userId')
                    res.body.data.should.have.property('fullName')
                    res.body.data.should.have.property('email')
                    res.body.data.should.have.property('role')
                    res.body.data.should.have.property('image')
                    done()
                })
        })
    })

    describe('/PUT update profile name', () => {
        it('It should update your profile name', (done) => {
            chai.request(server)
                .put('/user/update/profile/name')
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .send({
                    fullName: "Test"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('User update success')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('userId')
                    res.body.data.should.have.property('fullName')
                    res.body.data.should.have.property('email')
                    res.body.data.should.have.property('role')
                    res.body.data.should.have.property('image')
                    done()
                })
        })
    })

    describe('/PUT update user password', () => {
        it('It should update your user password', (done) => {
            chai.request(server)
                .put('/user/update/profile/password')
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .send({
                    password: "12345678",
                    passwordConfirmation: "12345678"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Password update success')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('userId')
                    res.body.data.should.have.property('fullName')
                    res.body.data.should.have.property('email')
                    res.body.data.should.have.property('role')
                    res.body.data.should.have.property('image')
                    done()
                })
        })
    })

    describe('/PUT update profile photo', () => {
        it('It should update your profile photo', (done) => {
            chai.request(server)
                .put('/user/update/profile/photo')
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .type('form')
                .attach('image', fs.readFileSync('/home/herubeno/Pictures/205582.jpg'), '205582.jpg')
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('User update success')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('userId')
                    res.body.data.should.have.property('fullName')
                    res.body.data.should.have.property('email')
                    res.body.data.should.have.property('role')
                    res.body.data.should.have.property('image')
                    done()
                })
        })
    })

    describe('/DELETE user', () => {
        it('It should delete your user', (done) => {
            chai.request(server)
                .delete('/user/delete')
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Profile delete success')
                    res.body.should.have.property('data').eql(null)
                    done()
                })

        })
    })
})

describe('MOVIE API', () => {

    describe('/GET all movie', () => {
        it('It should get all the movie', (done) => {
            chai.request(server)
                .get('/movie')
                .query({
                    page: 1,
                    limit: 10
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes get all the data')
                    res.body.should.have.property('posts')
                    res.body.should.have.property('totalPages')
                    res.body.should.have.property('currentPage')
                    done()
                })
        })
    })

    describe('/GET one movie', () => {
        it('It should get one movie', (done) => {
            movieTitle = "Marwoto"
            chai.request(server)
                .get('/movie/get/' + movieTitle)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Get one movie data success!')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('movieId')
                    res.body.data.should.have.property('title')
                    res.body.data.should.have.property('synopsis')
                    res.body.data.should.have.property('genre')
                    res.body.data.should.have.property('poster')
                    res.body.data.should.have.property('trailer')
                    res.body.data.should.have.property('director')
                    res.body.data.should.have.property('characters')
                    res.body.data.should.have.property('release_date')
                    done()
                })
        })
    })

    describe('/GET movie by genre', () => {
        it('It should get movie sort by genre', (done) => {
            chai.request(server)
                .get('/movie/findGenre')
                .query({
                    genre: "Anime",
                    page: 1,
                    limit: 10
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes get all the data')
                    res.body.should.have.property('data')
                    res.body.should.have.property('totalPages')
                    res.body.should.have.property('currentPage')
                    done()
                })
        })
    })

    describe('/GET movie by title', () => {
        it('It should get movie by title', (done) => {
            chai.request(server)
                .get('/movie/findTitle')
                .query({
                    title: "Naruto"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes get data')
                    res.body.should.have.property('data')
                    done()
                })
        })
    })

    describe('/POST movie', () => {
        it('It should create a movie', (done) => {
            chai.request(server)
                .post('/user/login')
                .send({
                    email: "admin@email.com",
                    password: "123456789"
                })
                .end((err, res) => {
                    authentication_key = res.body.token
                    chai.request(server)
                        .post('/movie/create')
                        .set({
                            Authorization: `Bearer ${authentication_key}`
                        })
                        .send({
                            title: "Sinchan",
                            synopsis: "Ada bocah kecil",
                            genre: "Anime",
                            characters: "Sinchan",
                            director: "Ga tau",
                            release_date: "2000"
                        })
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.be.an('object')
                            res.body.should.have.property('status').eql('Succes create new data')
                            res.body.should.have.property('data')
                            res.body.data.should.be.an('object')
                            res.body.data.should.have.property('movieId')
                            res.body.data.should.have.property('title')
                            res.body.data.should.have.property('synopsis')
                            res.body.data.should.have.property('genre')
                            res.body.data.should.have.property('poster')
                            res.body.data.should.have.property('trailer')
                            res.body.data.should.have.property('director')
                            res.body.data.should.have.property('characters')
                            res.body.data.should.have.property('release_date')
                            movieTitle = res.body.data.title
                            done()
                        })
                })
        })
    })

    describe('/PUT update movie', () => {
        it('It should update a movie', (done) => {
            chai.request(server)
                .put('/movie/update/' + movieTitle)
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .send({
                    synopsis: "Bocah kurang ajar ga tau diri",
                    genre: "Comedy",
                    characters: "Sinchan, Shiro, dll",
                    director: "Yoshito Usui",
                    release_date: "1992"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes updating data')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('movieId')
                    res.body.data.should.have.property('title')
                    res.body.data.should.have.property('synopsis')
                    res.body.data.should.have.property('genre')
                    res.body.data.should.have.property('poster')
                    res.body.data.should.have.property('trailer')
                    res.body.data.should.have.property('director')
                    res.body.data.should.have.property('characters')
                    res.body.data.should.have.property('release_date')
                    done()
                })
        })
    })

    describe('/PUT update movie poster', () => {
        it('It should update a movie poster', (done) => {
            chai.request(server)
                .put('/movie/update/poster/' + movieTitle)
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .type('form')
                .attach('poster', fs.readFileSync('/home/herubeno/Pictures/205582.jpg'), '205582.jpg')
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes updating Poster')
                    done()
                })
        })
    })

    describe('/PUT update movie trailer', () => {
        it('It should update a movie trailer', (done) => {
            chai.request(server)
                .put('/movie/update/trailer/' + movieTitle)
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .send({
                    trailer: "https://www.youtube.com/watch?v=qWaAKz2P2ZE"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes updating Trailer')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('movieId')
                    res.body.data.should.have.property('title')
                    res.body.data.should.have.property('synopsis')
                    res.body.data.should.have.property('genre')
                    res.body.data.should.have.property('poster')
                    res.body.data.should.have.property('trailer')
                    res.body.data.should.have.property('director')
                    res.body.data.should.have.property('characters')
                    res.body.data.should.have.property('release_date')
                    done()
                })
        })
    })

    describe('/DELETE movie', () => {
        it('It should delete a movie', (done) => {
            chai.request(server)
                .delete('/movie/delete/' + movieTitle)
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Succes delete data')
                    res.body.should.have.property('data').eql(null)
                    done()
                })
        })
    })
})

describe('REVIEW API', () => {

    describe('/GET review by movie', () => {
        it('It should get all review sort by movie', (done) => {
            chai.request(server)
                .get('/review/show/movie')
                .query({
                    movie_id: "5fe1e1bb7c15007890f32882",
                    page: 1,
                    limit: 10
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('post')
                    res.body.should.have.property('totalPages')
                    res.body.should.have.property('currentPage')
                    done()
                })
        })
    })

    describe('/GET review by user', () => {
        it('It should get all review sort by user', (done) => {
            chai.request(server)
                .get('/review/show/user')
                .query({
                    user_id: "5fe1d9513b94707703b35c0e",
                    page: 1,
                    limit: 10
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('post')
                    res.body.should.have.property('totalPages')
                    res.body.should.have.property('currentPage')
                    done()
                })
        })
    })

    describe('/GET review rating', () => {
        it('It should get review rating for movie', (done) => {
            chai.request(server)
                .get('/review/rating/movie')
                .query({
                    movie_id: "5fe1e1bb7c15007890f32882"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('data')
                    done()
                })
        })
    })

    describe('/GET one review', () => {
        it('It should get one review', (done) => {
            let reviewId = "5fe700691576f33ec4e2d778"
            chai.request(server)
                .get('/review/' + reviewId)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('success get review')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    res.body.data.should.have.property('comment')
                    res.body.data.should.have.property('rating')
                    res.body.data.should.have.property('user')
                    res.body.data.should.have.property('movie')
                    done()
                })
        })
    })

    describe('/POST a review', () => {
        it('It should create a review for one movie', (done) => {
            chai.request(server)
                .post('/review/create')
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .send({
                    movie_id: "5fe80e2b9969a41823cca033",
                    comment: "tiruannya naruto, ga seru",
                    rating: 3
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('status').eql('Review created')
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done()
                })
        })
    })

    describe('/PUT update review', () => {
        it('It should update a review', (done) => {
            let reviewId = "5fe700691576f33ec4e2d778"
            chai.request(server)
                .put('/review/update/' + reviewId)
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .send({
                    comment: "Seru sekali ya naruto ini, recommended lah pokoknya",
                    rating: 9
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Review update success')
                    res.body.should.have.property('data')
                    done()
                })
        })
    })

    describe('/DELETE review', () => {
        it('It should delete a review', (done) => {
            let reviewId = "5fe700691576f33ec4e2d778"
            chai.request(server)
                .delete('/review/delete/' + reviewId)
                .set({
                    Authorization: `Bearer ${authentication_key}`
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('review deleted')
                    res.body.should.have.property('data').eql(null)
                    done()
                })
        })
    })
})