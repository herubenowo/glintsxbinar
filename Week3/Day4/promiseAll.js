const fs = require(`fs`);

const readFile = options => file => new Promise((resolved, reject) => {
    fs.readFile(file, options, (error, content) => {
        if (error) {
            return reject(error)
        }
        return resolved(content)
    })
})

const writeFile = (file, content) => new Promise((resolved, reject) => {
    fs.writeFile(file, content, error => {
        if (error) {
            return reject(error)
        }
        return resolved(content)
    })
})

const read = readFile(`utf-8`)

async function gabungContent() {
    try {
        const result = await Promise.all([
            read(`contents/text1.txt`),
            read(`contents/text2.txt`),
            read(`contents/text3.txt`),
            read(`contents/text4.txt`),
            read(`contents/text5.txt`),
            read(`contents/text6.txt`),
            read(`contents/text7.txt`),
            read(`contents/text8.txt`),
            read(`contents/text9.txt`),
            read(`contents/text10.txt`),
        ])

        await writeFile(`contents/resultPromiseAll.txt`, result.join(` `))
    } catch (error) {
        throw error;
    }

    return read(`contents/resultPromiseAll.txt`)
}

gabungContent()
    .then(result => {
        console.log(`Success to read and write file, the result is: `, result);
    }).catch(error => {
        console.log(`Failed to read and write file, the result is: `, error);
    })