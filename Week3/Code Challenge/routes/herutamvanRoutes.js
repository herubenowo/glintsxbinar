const express = require(`express`)
const router = express.Router()
const HeruTamvanController = require(`../controllers/herutamvanController.js`)

router.get(`/herutamvan`, HeruTamvanController.heruTamvan)

module.exports = router