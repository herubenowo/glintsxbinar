const express = require(`express`)
const app = express()
const indexRoutes = require(`./routes/indexRoutes.js`)
const heruTamvanRoutes = require(`./routes/herutamvanRoutes.js`)

app.use(express.static('public'));

app.use(`/`, indexRoutes)

app.use(`/`, heruTamvanRoutes) // For code test please use "http://localhost:6969/herutamvan" on your web browser or click "Assignment Heru" on  Home Page! :)

app.listen(6969)