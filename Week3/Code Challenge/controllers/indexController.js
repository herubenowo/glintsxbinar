class IndexController {
    async home(req, res) {
        try {
            console.log(`Right now, you're accessing local host!`);
            res.render(`top.ejs`)
        } catch (e) {
            res.status(500).send(exception)
        }
    }

    async indexHome(req, res) {
        try {
            console.log(`Right now, you're accessing index!`);
            res.render(`index.ejs`)
        } catch (e) {
            res.status(500).send(exception)
        }
    }
}

module.exports = new IndexController