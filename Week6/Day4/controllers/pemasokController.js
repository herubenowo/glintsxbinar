const { pemasok } = require('../models')

class PemasokController {

    async getAll(req, res) {
        pemasok.find({}).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        pemasok.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        pemasok.create({
            nama: req.body.nama
        }).then(result => {
            res.json({
                status: "New pemasok's data has been created",
                data: result
            })
        })
    }

    async update(req, res) {
        pemasok.findOneAndUpdate({
            _id: req.params.id
        }, {
            nama: req.body.nama
        }).then(result => {
            res.json({
                status: "Pemasok's data has been successfully updated!",
                data: result
            })
        })
    }

    async delete(req, res) {
        pemasok.delete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "One of pemasok's data has been successfully deleted!",
                data: null
            })
        })
    }
}

module.exports = new PemasokController