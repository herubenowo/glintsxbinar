let mongoose = require("mongoose");
let { transaksi } = require('../models');
let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../index.js')
let should = chai.should()
let transaksi_id

chai.use(chaiHttp)

describe('Transaksi', () => {
    beforeEach((done) => {
        transaksi.remove({}, (err) => {
            done()
        })
    })
})

describe('/GET transaksi', () => {
    it('it should GET all the transaksi', (done) => {
        chai.request(server)
            .get('/transaksi')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('object')
                res.body.should.have.property('status')
                res.body.should.have.property('data')
                res.body.data.should.be.an('array')
                done()
            })
    })
})

describe('/POST transaksi', () => {
    it('should POST a transaksi', (done) => {
        chai.request(server)
            .post('/transaksi/create')
            .send({
                id_barang: "5fccb45e683964d75bd4d3bc",
                id_pelanggan: "5fccb406683964d75bd4d3b7",
                jumlah: 20
            })
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('object')
                res.body.should.have.property('status')
                res.body.should.have.property('data')
                res.body.data.should.be.an('object')
                res.body.data.should.have.property('_id')
                done()
            })
    })
})

describe('/GET/:id transaksi', () => {
    it('it should GET a transaksi', (done) => {
        let transaksiPost = {
            id_barang: "5fccb45e683964d75bd4d3bc",
            id_pelanggan: "5fccb406683964d75bd4d3b7",
            jumlah: 20
        }
        chai.request(server)
            /* POST */
            .post('/transaksi/create')
            .send(transaksiPost)
            .end((err, res) => {
                /* GET */
                transaksi_id = res.body.data._id
                chai.request(server)
                    .get('/transaksi/' + transaksi_id)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status');
                        res.body.should.have.property('data');
                        res.body.data.should.be.a('object');
                        res.body.data.should.have.property('_id').eql(transaksi_id);
                        done();
                    })
            });
    });
});

describe('/PUT/:id transaksi', () => {
    it('it should UPDATE a transaksi given the id', (done) => {
        let transaksiPost = {
            id_barang: "5fccb45e683964d75bd4d3bc",
            id_pelanggan: "5fccb3f6683964d75bd4d3b6",
            jumlah: 20
        }
        let transaksiPut = {
            id_barang: "5fccb45e683964d75bd4d3bc",
            id_pelanggan: "5fccb3f6683964d75bd4d3b6",
            jumlah: 30
        }
        /* POST */
        chai.request(server)
            .post('/transaksi/create')
            .send(transaksiPost)
            .end((err, res) => {
                /* PUT */
                transaksi_id = res.body.data._id
                chai.request(server)
                    .put('/transaksi/update/' + transaksi_id)
                    .send(transaksiPut)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status');
                        res.body.should.have.property('data');
                        res.body.data.should.be.a('object')
                        res.body.data.should.have.property('_id').eql(transaksi_id)
                        done()
                    })
            });
    });
});

describe('/DELETE/:id transaksi', () => {
    it('it should UPDATE a transaksi given the id', (done) => {
        let transaksiPost = {
            id_barang: "5fccb45e683964d75bd4d3bc",
            id_pelanggan: "5fccb3f6683964d75bd4d3b6",
            jumlah: 20
        }
        // CREATE
        chai.request(server)
            .post('/transaksi/create')
            .send(transaksiPost)
            .end((err, res) => {
                transaksi_id = res.body.data._id
                // DELETE
                chai.request(server)
                    .delete('/transaksi/delete/' + transaksi_id)
                    .end((err, res) => {
                        res.should.have.status(200)
                        res.body.should.be.a('object')
                        res.body.should.have.property('status')
                        res.body.should.have.property('data').eql(null)
                        done()
                    })
            })
    })
})