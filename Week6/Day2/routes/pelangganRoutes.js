const express = require('express')
const router = express.Router()
const pelangganValidator = require('../middlewares/validators/pelangganValidator.js')
const PelangganController = require('../controllers/pelangganController.js')
const auth = require('../middlewares/auth')
const passport = require('passport')

router.get('/', [passport.authenticate('pelanggan', { session : false })], PelangganController.getAll)
router.get('/:id', [passport.authenticate('pelanggan', { session : false })], pelangganValidator.getOne, PelangganController.getOne)
router.post('/create', [passport.authenticate('pelanggan', { session : false })], pelangganValidator.create, PelangganController.create)
router.put('/update/:id', [passport.authenticate('pelanggan', { session : false })], pelangganValidator.update, PelangganController.update)
router.delete('/delete/:id', [passport.authenticate('pelanggan', { session : false })], pelangganValidator.delete, PelangganController.delete)

module.exports = router