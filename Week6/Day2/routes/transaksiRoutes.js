const express = require('express')
const router = express.Router()
const transaksiValidator = require('../middlewares/validators/transaksiValidator.js')
const TransaksiController = require('../controllers/transaksiController.js')
const auth = require('../middlewares/auth')
const passport = require('passport')

router.get('/', [passport.authenticate('transaksi', { session : false })], TransaksiController.getAll)
router.get('/:id', [passport.authenticate('transaksi', { session : false })], transaksiValidator.getOne, TransaksiController.getOne)
router.post('/create', [passport.authenticate('transaksi', { session : false })], transaksiValidator.create, TransaksiController.create)
router.put('/update/:id', [passport.authenticate('transaksi', { session : false })], transaksiValidator.update, TransaksiController.update)
router.delete('/delete/:id', [passport.authenticate('transaksi', { session : false })], transaksiValidator.delete, TransaksiController.delete)

module.exports = router