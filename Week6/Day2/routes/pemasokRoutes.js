const express = require('express')
const router = express.Router()
const pemasokValidator = require('../middlewares/validators/pemasokValidator.js')
const PemasokController = require('../controllers/pemasokController.js')
const auth = require('../middlewares/auth')
const passport = require('passport')

router.get('/', [passport.authenticate('pemasok', { session : false })], PemasokController.getAll)
router.get('/:id', [passport.authenticate('pemasok', { session : false })], pemasokValidator.getOne, PemasokController.getOne)
router.post('/create', [passport.authenticate('pemasok', { session : false })], pemasokValidator.create, PemasokController.create)
router.put('/update/:id', [passport.authenticate('pemasok', { session : false })], pemasokValidator.update, PemasokController.update)
router.delete('/delete/:id', [passport.authenticate('pemasok', { session : false })], pemasokValidator.delete, PemasokController.delete)

module.exports = router