const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const {
    user
} = require('../../models/mysql')
const bcrypt = require('bcrypt')
const JWTstrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt

passport.use(
    'signup',
    new localStrategy({
            'usernameField': 'email',
            'passwordField': 'password',
            passReqToCallback: true
        },
        async (req, email, password, done) => {
            user.create({
                email: email,
                password: password,
                role: req.body.role
            }).then(newUser => { // THESE WILL BE SAVED TO REQ.USER
                return done(null, newUser, {
                    message: "User has been created successfully!"
                })
            }).catch(err => {
                return done(null, false, {
                    message: "Can't create user!"
                })
            })
        }
    )
)

passport.use(
    'login',
    new localStrategy({
            'usernameField': 'email',
            'passwordField': 'password'
        },
        async (email, password, done) => {
            const userLogin = await user.findOne({
                where: {
                    email: email
                }
            })

            if (!userLogin) {
                return done(null, false, {
                    message: "User email not found"
                })
            }

            const validate = await bcrypt.compare(password, userLogin.password)

            if (!validate) {
                return done(null, false, {
                    message: "Wrong password!"
                })
            }

            return done(null, userLogin, {
                message: "Login successful"
            })
        }
    )
)

passport.use(
    'transaksi',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find user depend on token.user.email
            const userLogin = await user.findOne({
                where: {
                    email: token.user.email
                }
            })

            // if user.role includes transaksi it will next
            if (userLogin.role.includes('transaksi')) {
                return done(null, token.user)
            }

            // if user.role not includes transaksi it will not authorization
            return done(null, false)
        }
    )
)

passport.use(
    'barang',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find user depend on token.user.email
            const userLogin = await user.findOne({
                where: {
                    email: token.user.email
                }
            })

            // if user.role includes transaksi it will next
            if (userLogin.role.includes('barang')) {
                return done(null, token.user)
            }

            // if user.role not includes transaksi it will not authorization
            return done(null, false)
        }
    )
)

passport.use(
    'pelanggan',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find user depend on token.user.email
            const userLogin = await user.findOne({
                where: {
                    email: token.user.email
                }
            })

            // if user.role includes transaksi it will next
            if (userLogin.role.includes('pelanggan')) {
                return done(null, token.user)
            }

            // if user.role not includes transaksi it will not authorization
            return done(null, false)
        }
    )
)

passport.use(
    'pemasok',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find user depend on token.user.email
            const userLogin = await user.findOne({
                where: {
                    email: token.user.email
                }
            })

            // if user.role includes transaksi it will next
            if (userLogin.role.includes('pemasok')) {
                return done(null, token.user)
            }

            // if user.role not includes transaksi it will not authorization
            return done(null, false)
        }
    )
)