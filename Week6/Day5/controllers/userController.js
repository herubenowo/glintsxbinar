const { user } = require('../models/mysql')
const passport = require('passport')
const jwt = require('jsonwebtoken')

class UserController {
    async signup(req, res) { 
        const body = {
            id: req.user.dataValues.id,
            email: req.user.dataValues.email
        }

        const token = await jwt.sign({
            user: body
        }, 'secret_password')

        res.status(200).json({
            message: "Signup Success!",
            token: token
        })
    }

    async login(req, res) {
        const body = {
            id: req.user.dataValues.id,
            email: req.user.dataValues.email
        }

        const token = await jwt.sign({
            user: body
        }, 'secret_password')

        res.status(200).json({
            message: "Login success!",
            token: token
        })
    }
}


module.exports = new UserController