const mongoose = require('mongoose')

const uri = 'mongodb://localhost:27017/penjualan_dev'

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })

const barang = require('./barang')
const pelanggan = require('./pelanggan')
const pemasok = require('./pemasok')
const transaksi = require('./transaksi')

module.exports = { barang, pelanggan, pemasok, transaksi }