const express = require('express')
const router = express.Router()
const passport = require('passport')
const auth = require('../middlewares/auth')
const userValidator = require('../middlewares/validators/userValidator.js')
const UserController = require('../controllers/userController.js')

router.post('/signup', [userValidator.signup, passport.authenticate('signup', { session: false })], UserController.signup) // RESTFUL API SO WE DONT NEED SESSION, IF WEB NEED SESSION
router.post('/login', [userValidator.login, passport.authenticate('login', { session: false })], UserController.login)

module.exports = router