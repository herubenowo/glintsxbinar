let mongoose = require("mongoose");
let { user } = require('../models/mysql');
let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../index.js')
let should = chai.should()
let authentication_key

chai.use(chaiHttp)

describe('user', () => {

    describe('/POST Sign Up', () => {
        it('It should make user and get authentication_key (jwt', () => {
            chai.request(server)
                .post('/signup')
                .send({
                    email: `herubenowo@example.com`,
                    password: `1234567890`,
                    passwordConfirmation: `1234567890`,
                    role: `transaksi`
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Signup Success!')
                    res.body.should.have.property('token')
                    done()
                })
        })
    })

    describe('/POST Log in', () => {
        it('It should make user login and get authentication_key (jwt', () => {
            chai.request(server)
                .post('/login')
                .send({
                    email: `herubenowo@example.com`,
                    password: `1234567890`,
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('message').eql('Login success!')
                    res.body.should.have.property('token')
                    authentication_key = res.body.token
                    done()
                })
        })
    })
})